<?php
function ubah_huruf($string){
    $output = "";
    for ($i=0; $i < strlen($string); $i++) { 
        $next_ch = $string[$i]; 
        ++$next_ch;
        $output = $output . $next_ch;
    }
    return $output;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu
echo "<br>";

?>